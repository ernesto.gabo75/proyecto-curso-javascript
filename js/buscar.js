$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");
	var resultadosBusqueda = $("#miLista");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/
		$.ajax({
			url: "http://api.themoviedb.org/3/search/movie?api_key=3356865d41894a2fa9bfa84b2b5f59bb&certification_country=MX&language=es&query=\""+palabra+"\"",

			success: function(answer) {
				setTimeout(desplegarResultadosBusqueda(answer, resultadosBusqueda), 3000);
			},
			
			error: function() {
				console.log("No se pudo realizar la busqueda...");
				resultadosBusqueda.empty();
				resultadosBusqueda.append("<h1>No sé pudo realizar la busqueda...</h1>");
			},
			
			beforeSend: function() {
				console.log('CARGANDO');
				resultadosBusqueda.empty();
				resultadosBusqueda.append('<div class="text-center"><img src="images/loading.gif" /></div>');
			}
		});

	});

});

function desplegarResultadosBusqueda(results, resultadosBusqueda) {
	resultadosBusqueda.empty();
	if(results.results.length == 0){
		resultadosBusqueda.append("<h1>No hay resultados encontrados...</h1>");
		return;
	}
	$.each(results.results, 
		function(index, element) {
			resultadosBusqueda.append(
				'<div class="media text-muted pt-3">'
				+  '<img style="max-width:50px;" class="mr-2 rounded" src="https://image.tmdb.org/t/p/w500' + element.poster_path + '"></img>'	
				+  '<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">'
				+    '<strong class="d-block text-gray-dark" data-toggle="modal" data-target="#modal'+index+'">'+element.title+'</strong>'
				+    element.overview
				+  '</p>'
				+'</div>'
			);
			crearModal(index, element.overview);
		}
	);
}

function crearModal(index, overview){
	var htmlModal='<div class="modal fade" id="modal'+index+'" tabindex="-1" role="dialog" aria-labelledby="modal'+index+'" aria-hidden="true">'
                  +'<div class="modal-dialog" role="document">'
                  +'<div class="modal-content">'
                  +'<div class="modal-header">'
                  +'<h5 class="modal-title" id="exampleModalLabel">Sinópsis</h5>'
                  +'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                  +'<span aria-hidden="true">&times;</span>'
                  +'</button>'
                  +'</div>'
                  +'<div class="modal-body">'
                  +overview
                  +'</div>'
                  +'<div class="modal-footer">'
                  +'<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'
                  +'</div>'
                  +'</div>'
                  +'</div>'
                  +'</div>';

    $('#modals').append(htmlModal);
}

