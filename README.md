# Curso Lenguaje de programación JavaScript
## Práctica Final: JQuery, Ajax, DOM, JSON
## Creado por Ernesto Gabriel Dávila Reyes
### Descripción
Este proyecto sirve para desplegar una pagina relacionada con peliculas, se hace uso del API de TMDB(The movie database),
se puede mostrar un catalogo de peliculas, cuenta con un formulario de registro, una sección donde se puede buscar una 
pélicula en particular
### Solución
Lo primero que hice fue arreglar el problema del API Key que era erroneo, de ahí fue bastante sencillo,
procedi a usar la misma solución que se usa en el catalogo, cambien un poco al hacer funciones para desplegar
los resultados esto para que el codigo no se vea tan mal, pero igual estas estan sujetas a cambios(implementar patrones de diseño).
